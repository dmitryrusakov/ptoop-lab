package main.plugin;

import main.shape.Shape;

public interface ShapePlugin {
    void handle(Shape shape);
}
