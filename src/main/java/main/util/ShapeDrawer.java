package main.util;

import main.shape.Shape;

import java.util.List;

public class ShapeDrawer {
    public void drawShape(Shape shape) {
        System.out.println(shape.getShapeData().getData());
    }

    public void drawChecksum(Shape shape) {
        int checksum = shape.getChecksum();
        if (checksum == 0) {
            System.out.println("checksum: n/a");
        } else {
            System.out.println("checksum:" + checksum);
        }
    }

    public void drawAllShapes(List<Shape> shapes) {
        for (int i = 0; i < shapes.size(); i++) {
            System.out.println("Shape #" + (i + 1));
            drawShape(shapes.get(i));
            drawChecksum(shapes.get(i));
            System.out.println();
        }
    }
}
